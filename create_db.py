from database import Base, engine
from models import FleetVehicle, Vehicle, Driver, Order

print("Creating database ...")

Base.metadata.create_all(engine)