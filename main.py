from fastapi import FastAPI, status, HTTPException
from pydantic import BaseModel
from typing import Optional, List
from database import SessionLocal
import models

app = FastAPI()


class FleetVehicle(BaseModel):  # serealize
    id: int
    fleet_name: str

    class Config:
        orm_mode = True


class Vehicle(BaseModel):
    id: int
    fleet_vehicle_id: int
    type: str
    name: str

    class Config:
        orm_mode = True


class Driver(BaseModel):
    id: int
    vehicle_id: int
    # fleet_vehicle_id: int
    name: str
    phone_number: str

    class Config:
        orm_mode = True


class Order(BaseModel):
    id: int
    # vehicle_id: int
    driver_id: int
    price: float

    class Config:
        orm_mode = True


class OrderOutput(BaseModel):
    driver_name: str
    vehicle_type: str
    vehicle_name: str
    price: float

    class Config:
        orm_mode = True


db = SessionLocal()


@app.get('/fleets', response_model=List[FleetVehicle], status_code=200)
def get_all_fleets():
    fleets = db.query(models.FleetVehicle).all()

    return fleets


@app.post('/fleets', response_model=FleetVehicle, status_code=status.HTTP_201_CREATED)
def create_an_fleet(fleet: FleetVehicle):
    db_fleet = db.query(models.FleetVehicle).filter(
        models.FleetVehicle.fleet_name == fleet.fleet_name).first()

    if db_fleet is not None:
        raise HTTPException(400, detail="Fleet already exists")

    new_fleet = models.FleetVehicle(fleet_name=fleet.fleet_name)

    db.add(new_fleet)
    db.commit()

    return new_fleet


@app.get('/fleets/{fleet_id}', response_model=FleetVehicle, status_code=status.HTTP_200_OK)
def get_an_item(fleet_id: int):
    fleet = db.query(models.FleetVehicle).filter(
        models.FleetVehicle.id == fleet_id).first()

    return fleet


@app.put('/fleets/{fleet_id}', response_model=FleetVehicle, status_code=status.HTTP_200_OK)
def update_an_item(fleet_id: int, fleet: FleetVehicle):
    fleet_to_update = db.query(models.FleetVehicle).filter(
        models.FleetVehicle.id == fleet_id).first()
    fleet_to_update.fleet_name = fleet.fleet_name

    db.commit()

    return fleet_to_update


@app.delete('/fleets/{fleet_id}', response_model=FleetVehicle, status_code=status.HTTP_200_OK)
def delete_item(fleet_id: int):
    fleet_to_delete = db.query(models.FleetVehicle).filter(
        models.FleetVehicle.id == fleet_id).first()

    if fleet_to_delete is None:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND, detail="Fleet not found!")

    db.delete(fleet_to_delete)
    db.commit()

    return fleet_to_delete

# Vehicle


@app.get('/vehicles', response_model=List[Vehicle], status_code=200)
def get_all_vehicles():
    vehicles = db.query(models.Vehicle).all()

    return vehicles


@app.post('/vehicles', response_model=Vehicle, status_code=status.HTTP_201_CREATED)
def create_a_vehicle(vehicle: Vehicle):
    check_fleet_id = db.query(models.FleetVehicle).filter(
        models.FleetVehicle.id == vehicle.fleet_vehicle_id).first()

    if check_fleet_id is None:
        raise HTTPException(
            400, 'fleet_vehicle_id is not present in fleet_vehicle table')

    new_vehicle = models.Vehicle(
        fleet_vehicle_id=vehicle.fleet_vehicle_id,
        type=vehicle.type,
        name=vehicle.name
    )

    db.add(new_vehicle)
    db.commit()

    return new_vehicle


@app.get('/vehicles/{vehicle_id}', response_model=Vehicle, status_code=status.HTTP_200_OK)
def get_a_vehicle(vehicle_id: int):
    vehicle = db.query(models.Vehicle).filter(
        models.Vehicle.id == vehicle_id).first()

    return vehicle


@app.put('/vehicles/{vehicle_id}', response_model=Vehicle, status_code=status.HTTP_200_OK)
def update_a_vehicle(vehicle_id: int, vehicle: Vehicle):
    check_vehicle_id = db.query(models.Vehicle).filter(
        models.FleetVehicle.id == vehicle.fleet_vehicle_id).first()

    if check_vehicle_id is None:
        raise HTTPException(
            404, detail="Fleet id is not present in fleet_vehicle table")

    vehicle_to_update = db.query(models.Vehicle).filter(
        models.Vehicle.id == vehicle_id).first()

    if vehicle_to_update is None:
        raise HTTPException(400, detail="vehicle_id not found!")

    vehicle_to_update.fleet_vehicle_id = vehicle.fleet_vehicle_id
    vehicle_to_update.type = vehicle.type
    vehicle_to_update.name = vehicle.name

    db.commit()

    return vehicle_to_update


@app.delete('/vehicles/{vehicle_id}', response_model=Vehicle, status_code=status.HTTP_200_OK)
def delete_vehicle(vehicle_id: int):
    vehicle_to_delete = db.query(models.Vehicle).filter(
        models.Vehicle.id == vehicle_id).first()

    if vehicle_to_delete is None:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND, detail="Vehicle not found!")

    db.delete(vehicle_to_delete)
    db.commit()

    return vehicle_to_delete

# Driver


@app.get('/drivers', response_model=List[Driver], status_code=200)
def get_all_drivers():
    drivers = db.query(models.Driver).all()

    return drivers


@app.get('/drivers/{driver_id}', response_model=Driver, status_code=status.HTTP_200_OK)
def get_a_driver(driver_id: int):
    driver = db.query(models.Driver).filter(
        models.Driver.id == driver_id).first()

    return driver


@app.post('/drivers', response_model=Driver, status_code=status.HTTP_201_CREATED)
def create_a_driver(driver: Driver):
    # check_fleet_hehicle_id = db.query(models.FleetVehicle).filter(
    #     models.FleetVehicle.id == driver.fleet_vehicle_id)
    check_vehicle_id = db.query(models.Vehicle).filter(
        models.Vehicle.id == driver.vehicle_id).first()

    if check_vehicle_id is None:
        raise HTTPException(status.HTTP_404_NOT_FOUND,
                            detail="Vehicle id not found in vehicle table!")

    # if check_fleet_hehicle_id is None:
    #     raise HTTPException(status.HTTP_404_NOT_FOUND,
    #                         detail="Fleet id not found in fleet table!")

    new_driver = models.Driver(
        vehicle_id=driver.vehicle_id,
        # fleet_vehicle_id=driver.fleet_vehicle_id,
        name=driver.name,
        phone_number=driver.phone_number
    )

    db.add(new_driver)
    db.commit()

    return new_driver


@app.put('/drivers/{driver_id}', response_model=Vehicle, status_code=status.HTTP_200_OK)
def update_a_driver(driver_id: int, driver: Driver):
    check_vehicle_id = db.query(models.Vehicle).filter(
        models.Vehicle.id == driver.vehicle_id).first()
    check_fleet_hehicle_id = db.query(models.FleetVehicle).filter(
        models.FleetVehicle.id == driver.fleet_vehicle_id)

    if check_vehicle_id is None:
        raise HTTPException(status.HTTP_404_NOT_FOUND,
                            detail="Vehicle id not found in vehicle table!")

    if check_fleet_hehicle_id is None:
        raise HTTPException(status.HTTP_404_NOT_FOUND,
                            detail="Fleet id not found in fleet table!")

    driver_to_update = db.query(models.Driver).filter(
        models.Driver.id == driver_id).first()

    if driver_to_update is None:
        raise HTTPException(status.HTTP_404_NOT_FOUND,
                            detail="Driver id not found in driver table!")

    driver_to_update.vehicle_id = driver.vehicle_id
    driver_to_update.fleet_vehicle_id = driver.fleet_vehicle_id
    driver_to_update.name = driver.name
    driver_to_update.phone_number = driver.phone_number

    db.commit()

    return driver_to_update


@app.delete('/drivers/{driver_id}', response_model=Driver, status_code=status.HTTP_200_OK)
def delete_driver(driver_id: int):
    driver_to_delete = db.query(models.Driver).filter(
        models.Driver.id == driver_id).first()

    if driver_to_delete is None:
        raise HTTPException(status.HTTP_404_NOT_FOUND,
                            "driver id not found in driver table!")

    db.delete(driver_to_delete)
    db.commit()

    return driver_to_delete

### Not adready done
@app.get('/orders', response_model=List[OrderOutput], status_code=200)
def get_orders():
    db_query = db.query(models.Driver.name, models.Vehicle.type, models.Vehicle.name, models.Order.price).join(
        models.Vehicle, models.Vehicle.id == models.Driver.vehicle_id).join(models.Order, models.Order.driver_id == models.Driver.id).all()
    print("driver_name: ", db_query)
    result = []
    for order in db_query:
        order_obj = OrderOutput(driver_name=order[0],
                                vehicle_type=order[1],
                                vehicle_name=order[2],
                                price=order[3],)
        result.append(order_obj)
    
    
    return result

@app.get('/orders/{order_id}', response_model=OrderOutput, status_code=status.HTTP_200_OK)
def get_orders(order_id: int):
    driver_name, vehicle_type, vehicle_name, price = db.query(models.Driver.name, models.Vehicle.type, models.Vehicle.name, models.Order.price).join(
        models.Vehicle, models.Vehicle.id == models.Driver.vehicle_id).join(models.Order, models.Order.driver_id == models.Driver.id).filter(models.Order.id == order_id).first()
    print("driver_name: ", driver_name)
    orderoutput = OrderOutput(driver_name=driver_name,
                              vehicle_type=vehicle_type,
                              vehicle_name=vehicle_name,
                              price=price)
    print("type: ", type(orderoutput))
    return orderoutput

@app.post('/orders', response_model=Order, status_code=status.HTTP_201_CREATED)
def create_an_order(order: Order):
    check_driver_id = db.query(models.Driver).filter(models.Driver.id == order.driver_id).first()
    
    if check_driver_id is None:
        raise HTTPException(status.HTTP_404_NOT_FOUND, detail="Driver id not found in driver table")
    print("order.driver_id: ", order.driver_id)
    print("order.price: ", order.price)
    new_order = models.Order(driver_id=order.driver_id, price=order.price)
    db.add(new_order)
    db.commit()
    
    return new_order

@app.delete('/orders/{order_id}', response_model=Order, status_code=status.HTTP_200_OK)
def delete_an_order(order_id: int):
    order_to_delete = db.query(models.Order).filter(models.Order.id == order_id).first()
    
    if order_to_delete is None:
        raise HTTPException(status.HTTP_404_NOT_FOUND, detail="Order id not found in order table !1")
    
    db.delete(order_to_delete)
    db.commit()
    
    return order_to_delete