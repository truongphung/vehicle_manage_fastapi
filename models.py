from database import Base
from sqlalchemy import ForeignKey, String, Boolean, Integer, Column, Text, Float

class FleetVehicle(Base):
    __tablename__ = "fleet_vehicle"
    
    id = Column(Integer, primary_key=True, index=True)
    fleet_name = Column(String(255), unique=True, nullable=False)
    
 
class Vehicle(Base):
    __tablename__ = "vehicle"
    
    id = Column(Integer, primary_key=True, index=True)
    fleet_vehicle_id = Column(Integer, ForeignKey("fleet_vehicle.id", ondelete='CASCADE')) #Foreign key added
    type = Column(String(100), nullable=False) # Car or motobike
    name = Column(String(100), nullable=False) # mescedes, honda, yamaha
    
    def __repr__(self):
        return f"<Vehicle type: {type}>"
    
class Driver(Base):
    __tablename__ = "driver"
    
    id = Column(Integer, primary_key=True, index=True)
    vehicle_id = Column(Integer, ForeignKey("vehicle.id", ondelete='CASCADE'))
    # fleet_vehicle_id = Column(Integer, ForeignKey("fleet_vehicle.id"))
    name = Column(String(255), nullable=False)
    phone_number = Column(String(10), nullable=False)
    
    
class Order(Base):
    __tablename__ = "order"
    
    id = Column(Integer, primary_key=True, autoincrement="auto")
    # vehicle_id = Column(Integer, ForeignKey("vehicle.id"))
    driver_id = Column(Integer, ForeignKey("driver.id", ondelete='CASCADE'))
    price = Column(Float, nullable=False)


    